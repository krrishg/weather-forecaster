import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class WeatherForecaster {
    public static void main(String[] args) { 
        String place;
		
		Map<String, String> weatherAttributes = new HashMap<String, String>();
		weatherAttributes.put("min", "न्युनतम");
		weatherAttributes.put("max", "अधिक्तम");
		weatherAttributes.put("rain", "वर्षा");

        Scanner in = new Scanner(System.in);
        
        System.out.print("ठाउँ:");
        place = in.nextLine();
        
        try {
            URL url = new URL("https://nepal-weather-api.herokuapp.com/api/?placenp=" + place);
            HttpURLConnection request = (HttpURLConnection)url.openConnection();
            request.connect();
            
            JSONParser parser = new JSONParser();
            JSONObject obj = (JSONObject) parser.parse(new InputStreamReader((
                    InputStream)request.getContent()));
			Map<String, String> weather = new HashMap<String, String>(); 
			weather.putAll((Map)obj);
			
			for (String property: weather.keySet()) {
				if (weather.get("status").equals("true")) {
					if(weatherAttributes.containsKey(property)) {
						System.out.println(weatherAttributes.get(property) + " : " + weather.get(property));
					}
				} else {
					System.out.println("ठाउँ भेटिएन।उपलब्ध ठाउँहरु:");
					System.out.println("'Dadeldhura','Dipayal','Dhangadi','Birendranagar','Nepalgunj','Jumla','Dang','Pokhara','Bhairahawa','Simara','Kathmandu','Okhaldhunga','Taplejung','Dhankuta','Biratnagar','Jomsom','Dharan','Lumle','Janakpur','Jiri'");
					break;
				}
			}
        } catch (IOException | ParseException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
